import React from 'react';
import 'react-native';
import renderer from 'react-test-renderer';

import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import EnterprisesScreen from '../src/screens/Enterprises';

const mockStore = configureStore([]);

describe('EnterprisesScreen', () => {
  let store;
  let component;

  beforeEach(() => {
    store = mockStore({
      companies: { companies: null, loading: false },
    });

    component = renderer.create(
      <Provider store={store}>
        <EnterprisesScreen />
      </Provider>,
    );
  });

  it('should renders EnterprisesScreen', () => {
    expect(component.root.findByType('View').type).toBe('View');
  });
});
