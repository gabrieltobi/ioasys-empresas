import React from 'react';
import 'react-native';
import renderer from 'react-test-renderer';

import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import LoginScreen from '../src/screens/Login';

const mockStore = configureStore([]);

describe('LoginScreen', () => {
  let store;
  let component;

  beforeEach(() => {
    store = mockStore({
      auth: { user: null },
    });

    component = renderer.create(
      <Provider store={store}>
        <LoginScreen />
      </Provider>,
    );
  });

  it('should renders LoginScreen', () => {
    expect(component.root.findByType('View').type).toBe('View');
  });
});
