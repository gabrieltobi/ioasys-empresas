import React from 'react';
import 'react-native';
import renderer from 'react-test-renderer';

import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import EnterpriseScreen from '../src/screens/Enterprise';

const mockStore = configureStore([]);

describe('EnterpriseScreen', () => {
  let store;
  let component;

  beforeEach(() => {
    store = mockStore({
      company: { company: null },
    });

    component = renderer.create(
      <Provider store={store}>
        <EnterpriseScreen data={{ id: 1, enterprise_type: {} }} />
      </Provider>,
    );
  });

  it('should renders EnterpriseScreen', () => {
    expect(component.root.findByType('View').type).toBe('View');
  });
});
