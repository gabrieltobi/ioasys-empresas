![N|Solid](src/assets/images/logo.png)

# Desafio React Native - ioasys

Este repositório é uma implementação do desafio "Empresas" proposto pela **ioasys**. A descrição do desafio pode ser encontrada [aqui](https://bitbucket.org/gabrieltobi/ioasys-empresas/src/3fba58cbf87dac6704dcb5649da4c6ae7f7999dd/README.md).

---
### Observações
* O projeto não foi testado no _iOS_.

### Pontos Relevantes
* Os 4 endpoints foram utilizados.
* Foi utilizado React Native.
* A aplicação possui mais de uma tela.
* O README.md contém uma pequena justificativa de cada dependência.
* O README.md contém instruções de como executar a aplicação.
* Foi utilizado Redux / Redux Saga.
* Foi utilizado um _linter_ (eslint) e um _formatter_ (prettier).
* Foram feitos testes genéricos.

### Justificativas das bibliotecas
* _@react-native-community/async-storage_ - Persistir os dados de autenticação.
* _axios_ - Realizar requisições HTTP.
* _react-hook-form_ - Facilitar a criação de formulários.
* _react-native-navigation_ - Controlar a navegação do aplicativo.
* _react-native-navigation-register-screens_ - Facilitador na configuração do _react-native-navigation_.
* _react-native-paper_ - Componentes visuais.
* _react-native-vector-icons_ - Requerido pelo _react-native-paper_.
* _react-redux_ - Requerido pelo _redux_ para se utilizar com _React_.
* _redux_ - Controle de estado da aplicação.
* _redux-logger_ - Assistente de _debug_ no modo de desenvolvimento.
* _redux-persist_ - Facilitador do processo de persistir dados.
* _redux-saga_ - Controlar as requisições assíncronas e integrá-las com o _redux_.
* _redux-mock-store_ - Auxiliar na criação de testes unitários.

### Como executar o projeto
* Clone este repositório.
* Acesse a pasta do projeto através do terminal de sua preferência.
* Execute o comando _npm i_.
* Execute o comando _npm run android_.