import React from 'react';
import { registerScreens } from 'react-native-navigation-register-screens';
import { Provider as PaperProvider } from 'react-native-paper';
import { Provider as ReduxProvider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import Theme from './src/app/theme';
import configureStore from './src/app/store';
import setupAxios from './src/app/axiosConfig';

import { screensAndComponents } from './src/app/navigation';

const { store, persistor } = configureStore();

setupAxios(store);

registerScreens(screensAndComponents, (Component) => (props) => (
  <ReduxProvider store={store}>
    <PaperProvider theme={Theme}>
      <PersistGate persistor={persistor}>
        <Component {...props} />
      </PersistGate>
    </PaperProvider>
  </ReduxProvider>
));
