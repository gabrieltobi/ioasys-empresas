import { combineReducers } from 'redux';

import authReducer from '../data/auth/reducer';
import companiesReducer from '../data/companies/reducer';
import companyReducer from '../data/company/reducer';

const rootReducer = combineReducers({
  auth: authReducer,
  companies: companiesReducer,
  company: companyReducer,
});

export default rootReducer;
