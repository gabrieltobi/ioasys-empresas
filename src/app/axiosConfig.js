import axios from 'axios';

const setupAxios = (store) => {
  axios.defaults.baseURL = 'https://empresas.ioasys.com.br/api/v1';

  axios.interceptors.request.use(
    (request) => {
      const { auth } = store.getState();

      if (auth && auth.headers) {
        request.headers = {
          ...request.headers,
          ...auth.headers,
        };
      }

      return request;
    },
    (error) => {
      return Promise.reject(error);
    },
  );
};

export default setupAxios;
