import { DarkTheme } from 'react-native-paper';

export const Theme = {
  ...DarkTheme,
  roundness: 2,
  colors: {
    ...DarkTheme.colors,
    primary: '#00b7b9',
    background: 'transparent',
  },
};

export default Theme;
