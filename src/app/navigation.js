import { Navigation } from 'react-native-navigation';
import { Colors } from 'react-native-paper';

import LogoutButton from '../components/LogoutButton';

import LoginScreen from '../screens/Login';
import EnterprisesScreen from '../screens/Enterprises';
import EnterpriseScreen from '../screens/Enterprise';

Navigation.setDefaultOptions({
  statusBar: {
    backgroundColor: '#022938',
  },
  topBar: {
    backButton: {
      color: Colors.white,
    },
    title: {
      color: Colors.white,
    },
    background: {
      color: '#033447',
    },
  },
});

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: 'Login',
            },
          },
        ],
      },
    },
  });
});

export const screensAndComponents = [
  // Telas
  LoginScreen,
  EnterprisesScreen,
  EnterpriseScreen,

  // Componentes
  LogoutButton,
];
