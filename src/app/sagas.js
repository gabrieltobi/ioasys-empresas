import { all } from 'redux-saga/effects';

import { doAuthWatcherSaga } from '../data/auth/saga';
import { fetchCompaniesWatcherSaga } from '../data/companies/saga';
import { fetchCompanyWatcherSaga } from '../data/company/saga';

export default function* rootSaga() {
  yield all([
    doAuthWatcherSaga(),
    fetchCompaniesWatcherSaga(),
    fetchCompanyWatcherSaga(),
  ]);
}
