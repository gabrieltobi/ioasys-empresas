import axios from 'axios';
import { takeLatest, call, put } from 'redux-saga/effects';

import { updateCompaniesError, updateCompanies } from './actionCreators';

function fetchCompanies(params) {
  return axios.request({
    url: '/enterprises',
    params,
  });
}

function* fetchCompaniesEffectSaga(action) {
  try {
    let { data } = yield call(fetchCompanies, action.payload);

    yield put(updateCompanies(data.enterprises));
  } catch (e) {
    yield put(updateCompaniesError(e.response.data.errors[0]));
  }
}

export function* fetchCompaniesWatcherSaga() {
  yield takeLatest('FETCH_COMPANIES_WATCHER', fetchCompaniesEffectSaga);
}
