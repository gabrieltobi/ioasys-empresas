const initialState = {
  companies: null,
  loading: false,
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'FETCH_COMPANIES_WATCHER':
      return {
        ...state,
        loading: true,
        error: null,
      };

    case 'UPDATE_COMPANIES':
      return {
        ...state,
        companies: action.payload,
        loading: false,
      };

    case 'UPDATE_COMPANIES_ERROR':
      return {
        ...state,
        companies: null,
        error: action.payload,
        loading: false,
      };

    default:
      return state;
  }
}
