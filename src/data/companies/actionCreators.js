export function fetchCompaniesWatcher(params) {
  return { type: 'FETCH_COMPANIES_WATCHER', payload: params };
}

export function updateCompanies(companies) {
  return { type: 'UPDATE_COMPANIES', payload: companies };
}

export function updateCompaniesError(error) {
  return { type: 'UPDATE_COMPANIES_ERROR', payload: error };
}
