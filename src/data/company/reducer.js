const initialState = {
  company: null,
  loading: false,
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'FETCH_COMPANY_WATCHER':
      return {
        ...state,
        company: null,
        loading: true,
        error: null,
      };

    case 'UPDATE_COMPANY':
      return {
        ...state,
        company: action.payload,
        loading: false,
      };

    case 'UPDATE_COMPANY_ERROR':
      return {
        ...state,
        error: action.payload,
        loading: false,
      };

    default:
      return state;
  }
}
