export function fetchCompanyWatcher(id) {
  return { type: 'FETCH_COMPANY_WATCHER', payload: id };
}

export function updateCompany(company) {
  return { type: 'UPDATE_COMPANY', payload: company };
}

export function updateCompanyError(error) {
  return { type: 'UPDATE_COMPANY_ERROR', payload: error };
}
