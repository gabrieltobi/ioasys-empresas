import axios from 'axios';
import { takeLatest, call, put } from 'redux-saga/effects';

import { updateCompanyError, updateCompany } from './actionCreators';

function fetchCompany(id) {
  return axios.request({
    url: `/enterprises/${id}`,
  });
}

function* fetchCompanyEffectSaga(action) {
  try {
    let { data } = yield call(fetchCompany, action.payload);

    yield put(updateCompany(data.enterprise));
  } catch (e) {
    yield put(updateCompanyError(e.response.data.errors[0]));
  }
}

export function* fetchCompanyWatcherSaga() {
  yield takeLatest('FETCH_COMPANY_WATCHER', fetchCompanyEffectSaga);
}
