export function doAuthWatcher(authParams) {
  return { type: 'DO_AUTH_WATCHER', payload: authParams };
}

export function updateUser(userAndHeaders) {
  return { type: 'UPDATE_USER', payload: userAndHeaders };
}

export function updateAuthError(error) {
  return { type: 'UPDATE_AUTH_ERROR', payload: error };
}

export function logout() {
  return { type: 'LOGOUT' };
}
