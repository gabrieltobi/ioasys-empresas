const initialState = {
  user: null,
  headers: null,
  loading: false,
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'DO_AUTH_WATCHER':
      return {
        ...state,
        loading: true,
        error: null,
      };

    case 'UPDATE_USER':
      return {
        ...state,
        user: action.payload.user,
        headers: action.payload.headers,
        loading: false,
      };

    case 'UPDATE_AUTH_ERROR':
      return {
        ...state,
        user: null,
        headers: null,
        error: action.payload,
        loading: false,
      };

    case 'LOGOUT':
      return initialState;

    default:
      return state;
  }
}
