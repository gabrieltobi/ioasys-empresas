import axios from 'axios';
import { takeLatest, call, put } from 'redux-saga/effects';

import { updateAuthError, updateUser } from './actionCreators';

function loginApi(authParams) {
  return axios.request({
    method: 'post',
    url: '/users/auth/sign_in',
    data: authParams,
  });
}

function* doAuthEffectSaga(action) {
  try {
    let { data, headers } = yield call(loginApi, action.payload);

    yield put(
      updateUser({
        user: data.investor,
        headers: {
          'access-token': headers['access-token'],
          client: headers.client,
          uid: headers.uid,
        },
      }),
    );
  } catch (e) {
    yield put(updateAuthError(e.response.data.errors[0]));
  }
}

export function* doAuthWatcherSaga() {
  yield takeLatest('DO_AUTH_WATCHER', doAuthEffectSaga);
}
