import React from 'react';
import { ImageBackground } from 'react-native';

import styles from './styles';

const background = require('../../assets/images/background.jpg');

const AppBackground = ({ children }) => {
  return (
    <ImageBackground
      source={background}
      style={styles.background}
      blurRadius={20}>
      {children}
    </ImageBackground>
  );
};

export default AppBackground;
