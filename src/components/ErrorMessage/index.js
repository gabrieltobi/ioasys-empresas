import React from 'react';
import { Text } from 'react-native';
import { useTheme } from 'react-native-paper';

const ErrorMessage = ({ error, messageOnly }) => {
  const {
    colors: { error: colorError },
  } = useTheme();

  if (!error) {
    return null;
  }

  return (
    <Text style={{ color: colorError }}>
      {messageOnly ? error : error.message}
    </Text>
  );
};

export default ErrorMessage;
