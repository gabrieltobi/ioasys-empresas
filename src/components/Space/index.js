import React from 'react';
import { View } from 'react-native';

const Space = ({ height, width }) => {
  const style = { height, width };
  return <View style={style} />;
};

export default Space;
