import React from 'react';
import { Navigation } from 'react-native-navigation';
import { IconButton, Colors } from 'react-native-paper';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { logout } from '../../data/auth/actionCreators';

const LogoutButton = (props) => {
  const doLogout = () => {
    props.logout();

    return Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component: {
                name: 'Login',
              },
            },
          ],
        },
      },
    });
  };

  return (
    <React.Fragment>
      <IconButton
        icon="logout"
        color={Colors.white}
        size={24}
        onPress={doLogout}
      />
    </React.Fragment>
  );
};

LogoutButton.screenName = 'LogoutButton';

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ logout }, dispatch);
};

export default connect(null, mapDispatchToProps)(LogoutButton);
