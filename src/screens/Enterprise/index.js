import React, { useEffect } from 'react';
import { Image, ScrollView, View } from 'react-native';
import {
  Caption,
  Headline,
  Paragraph,
  Subheading,
  Title,
} from 'react-native-paper';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import styles from './styles';
import AppBackground from '../../components/AppBackground';
import Space from '../../components/Space';

import { fetchCompanyWatcher } from '../../data/company/actionCreators';

const EnterpriseScreen = (props) => {
  const {
    data,
    fetchCompany,
    company: { company },
  } = props;

  const { id } = data;
  const companyData = company || data;

  useEffect(() => {
    fetchCompany(id);
  }, [fetchCompany, id]);

  return (
    <AppBackground>
      <ScrollView>
        <Image
          source={{ uri: `https://empresas.ioasys.com.br${companyData.photo}` }}
          style={styles.image}
        />

        <View style={styles.dataWrapper}>
          <Headline>{companyData.enterprise_name}</Headline>

          <View style={styles.info}>
            <Subheading>
              {companyData.enterprise_type.enterprise_type_name}
            </Subheading>
            <Caption>
              {companyData.city} ({companyData.country})
            </Caption>
          </View>

          <Space height={20} />

          <Title>Descrição</Title>
          <Paragraph>{companyData.description}</Paragraph>
        </View>
      </ScrollView>
    </AppBackground>
  );
};

EnterpriseScreen.screenName = 'Enterprise';

const mapStateToProps = ({ company }) => ({
  company,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      fetchCompany: fetchCompanyWatcher,
    },
    dispatch,
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(EnterpriseScreen);
