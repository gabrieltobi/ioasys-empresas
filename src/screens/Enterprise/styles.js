import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  image: {
    height: 180,
    width: '100%',
  },
  dataWrapper: {
    padding: 10,
  },
  info: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
