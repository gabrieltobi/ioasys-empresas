import React, { useEffect, useCallback, useRef } from 'react';
import { Image, SafeAreaView, View } from 'react-native';
import { Navigation } from 'react-native-navigation';
import {
  ActivityIndicator,
  Button,
  TextInput,
  useTheme,
} from 'react-native-paper';
import { useForm } from 'react-hook-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const logo = require('../../assets/images/logo.png');

import styles from './styles';
import AppBackground from '../../components/AppBackground';
import Space from '../../components/Space';
import ErrorMessage from '../../components/ErrorMessage';

import { doAuthWatcher } from '../../data/auth/actionCreators';

const LoginScreen = (props) => {
  const { doAuth, auth } = props;

  const { roundness } = useTheme();

  const { register, handleSubmit, errors, setValue } = useForm();
  const inputRefs = [useRef(), useRef()];

  const goToEnterprises = useCallback((data) => {
    return Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component: {
                name: 'Enterprises',
              },
            },
          ],
        },
      },
    });
  }, []);

  useEffect(() => {
    register('email', {
      required: 'Informe o e-mail',
      pattern: {
        value: /\S+@\S+\.\S+/,
        message: 'Informe um e-mail válido',
      },
    });
    register('password', { required: 'Informe a senha' });
  }, [register]);

  useEffect(() => {
    if (auth.user) {
      goToEnterprises();
    }
  }, [auth.user, goToEnterprises]);

  return (
    <AppBackground>
      <SafeAreaView style={styles.safeAreaView}>
        <View style={styles.screenWrapper}>
          <View style={[styles.logoWrapper, { borderRadius: roundness }]}>
            <Image source={logo} style={styles.logo} />
          </View>

          <View style={[styles.formWrapper, { borderRadius: roundness }]}>
            <TextInput
              ref={inputRefs[0]}
              mode="outlined"
              placeholder="E-mail"
              onChangeText={(value) => setValue('email', value)}
              autoCompleteType="email"
              keyboardType="email-address"
              autoCapitalize="none"
              returnKeyType="next"
              error={!!errors.email}
              onSubmitEditing={() => inputRefs[1].current.forceFocus()}
            />
            <ErrorMessage error={errors.email} />

            <TextInput
              ref={inputRefs[1]}
              mode="outlined"
              placeholder="Senha"
              onChangeText={(value) => setValue('password', value)}
              autoCompleteType="password"
              secureTextEntry
              error={!!errors.password}
              onSubmitEditing={(e) => handleSubmit(doAuth)(e)}
            />
            <ErrorMessage error={errors.password} />

            <Space height={30} />

            {!auth.loading ? (
              <Button mode="contained" onPress={handleSubmit(doAuth)}>
                Acessar
              </Button>
            ) : (
              <React.Fragment>
                <ActivityIndicator />
                <Space height={6} />
              </React.Fragment>
            )}

            {auth.error && (
              <View style={styles.apiError}>
                <ErrorMessage error={auth.error} messageOnly />
              </View>
            )}

            <Space height={6} />
          </View>

          <View style={styles.placeholderWrapper} />
        </View>
      </SafeAreaView>
    </AppBackground>
  );
};

LoginScreen.screenName = 'Login';
LoginScreen.options = {
  statusBar: {
    translucent: true,
    drawBehind: true,
  },
  topBar: {
    visible: false,
  },
};

const mapStateToProps = ({ auth }) => ({
  auth,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ doAuth: doAuthWatcher }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
