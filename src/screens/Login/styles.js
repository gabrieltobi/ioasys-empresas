import { StatusBar, StyleSheet } from 'react-native';

export default StyleSheet.create({
  safeAreaView: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
  },
  screenWrapper: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'space-around',
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  logoWrapper: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    marginVertical: 10,
  },
  logo: {
    width: 220,
    height: 90,
    resizeMode: 'contain',
  },
  formWrapper: {
    width: '100%',
    marginVertical: 10,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  apiError: {
    alignItems: 'center',
    paddingTop: 12,
  },
  placeholderWrapper: {
    marginVertical: 10,
  },
});
