import React, { useEffect } from 'react';
import { FlatList, View } from 'react-native';
import {
  List,
  useTheme,
  Avatar,
  Searchbar,
  Subheading,
} from 'react-native-paper';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import styles from './styles';
import AppBackground from '../../components/AppBackground';
import Space from '../../components/Space';

import { fetchCompaniesWatcher } from '../../data/companies/actionCreators';

const EnterprisesScreen = (props) => {
  const {
    componentId,
    fetchCompanies,
    companies: { companies, loading },
  } = props;

  const { roundness } = useTheme();

  const goToCompanyDetail = (company) => {
    Navigation.push(componentId, {
      component: {
        name: 'Enterprise',
        passProps: { data: company },
        options: {
          topBar: {
            title: {
              text: company.enterprise_name,
            },
          },
        },
      },
    });
  };

  const search = (text) => {
    fetchCompanies({ name: text });
  };

  const renderEmptyList = () => {
    if (!loading) {
      return (
        <View style={styles.emptyList}>
          <Subheading>Nada encontrado</Subheading>
        </View>
      );
    }

    return null;
  };

  useEffect(() => {
    fetchCompanies();
  }, [fetchCompanies]);

  return (
    <AppBackground>
      <Searchbar
        style={styles.searchbar}
        placeholder="Pesquisar"
        onChangeText={search}
      />

      <FlatList
        style={styles.list}
        data={companies}
        keyExtractor={(item) => item.id.toString()}
        onRefresh={fetchCompanies}
        refreshing={loading}
        contentContainerStyle={styles.listContent}
        ItemSeparatorComponent={() => <Space height={6} />}
        ListEmptyComponent={renderEmptyList}
        renderItem={({ item }) => {
          return (
            <List.Item
              style={[styles.companyCard, { borderRadius: roundness }]}
              title={item.enterprise_name}
              description={item.description}
              onPress={() => goToCompanyDetail(item)}
              left={(leftProps) => {
                return (
                  <Avatar.Image
                    {...leftProps}
                    size={68}
                    source={{
                      uri: `https://empresas.ioasys.com.br${item.photo}`,
                    }}
                  />
                );
              }}
            />
          );
        }}
      />
    </AppBackground>
  );
};

EnterprisesScreen.screenName = 'Enterprises';
EnterprisesScreen.options = {
  topBar: {
    title: {
      text: 'Empresas',
    },
    rightButtons: [{ id: '1', component: { name: 'LogoutButton' } }],
  },
};

const mapStateToProps = ({ companies }) => ({
  companies,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      fetchCompanies: fetchCompaniesWatcher,
    },
    dispatch,
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(EnterprisesScreen);
