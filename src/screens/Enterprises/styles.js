import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  searchbar: {
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  listContent: {
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  companyCard: {
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  emptyList: {
    alignItems: 'center',
  },
});
